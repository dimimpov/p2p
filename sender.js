const { Buffer } = require("buffer");
const createNode = require("./node");

(async () => {
  const topic = "hello";

  const node = await createNode();

  const addresses = node.peerInfo.multiaddrs.toArray();
  console.log("listening on addresses:");
  addresses.forEach((addr) => {
    console.log(`${addr.toString()}/p2p/${node.peerInfo.id.toB58String()}`);
  });

  setInterval(() => {
    node.pubsub.publish(topic, Buffer.from("Can you hear me ?"));
  }, 1000);
})();
