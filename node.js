const Libp2p = require("libp2p");
const TCP = require("libp2p-tcp");
const Mplex = require("libp2p-mplex");
const { NOISE } = require("libp2p-noise");
const SECIO = require("libp2p-secio");
const Gossipsub = require("libp2p-gossipsub");
const multiaddr = require("multiaddr");
const MulticastDNS = require("libp2p-mdns");

const createNode = async () => {
  const node = await Libp2p.create({
    modules: {
      transport: [TCP],
      streamMuxer: [Mplex],
      connEncryption: [NOISE, SECIO],
      peerDiscovery: [MulticastDNS],
      pubsub: Gossipsub,
    },
    config: {
      peerDiscovery: {
        mdns: {
          interval: 20e3,
          enabled: true,
        },
      },
    },
  });

  const listenAddress = multiaddr(`/ip4/127.0.0.1/tcp/0`);
  node.peerInfo.multiaddrs.add(listenAddress);

  await node.start();
  return node;
};

module.exports = createNode;
