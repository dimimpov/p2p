const { Buffer } = require("buffer");
const createNode = require("./node");

(async () => {
  const topic = "hello";

  const node = await createNode();

  node.on("peer:discovery", async (peer) => {
    console.log("Discovered:", peer.id.toB58String());
    await node.dial(peer);
    await node.pubsub.subscribe(topic, (msg) => {
      console.log(`message received: ${msg.data.toString()}`);
    });
  });

  // print out listening addresses
  const addresses = node.peerInfo.multiaddrs.toArray();
  console.log("listening on addresses:");
  addresses.forEach((addr) => {
    console.log(`${addr.toString()}/p2p/${node.peerInfo.id.toB58String()}`);
  });
})();
