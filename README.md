## Peer to Peer Network inside the same LAN

# A example of how you can share data between nodes in the same LAN using the lib2p2 library

node sender.js

node receiver.js
